SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=opsperator/rspamd
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: teststart
teststart:
	docker rm -f rspamd || true; \
	docker run \
	    --name rspamd \
	    -e DEBUG=yay \
	    -d $(IMAGE)
	sleep 60

.PHONY: test
test:
	if ! docker ps | grep rspamd >/dev/null; then \
	    make teststart; \
	fi
	set -x; \
        works1=false; \
        works2=false; \
        works3=false; \
        for try in 1 2 3 4 5; \
        do \
            echo "=== Test $$try/5 ==="; \
            if docker exec rspamd /bin/bash -c 'echo abc >/dev/tcp/127.0.0.1/11332'; then \
                works1=true; \
                if docker exec rspamd /bin/bash -c 'echo abc >/dev/tcp/127.0.0.1/11333'; then \
                    works2=true; \
                    if docker exec rspamd /bin/bash -c 'echo abc >/dev/tcp/127.0.0.1/11334'; then \
                        works3=true; \
                        break; \
                    fi; \
                fi; \
            elif test $$try -eq 5; then \
                break; \
            fi; \
            echo retrying in 30 seconds; \
            sleep 30; \
        done; \
	echo works1=$$works1 works2=$$works2 works3=$$works3

.PHONY: bmtest
bmtest:
	@@./test.sh

.PHONY: run
run:
	docker rm -f rspamd || true; \
	docker run \
	    --name rspamd \
	    -e DEBUG=yay \
	    $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in configmap service statefulset deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "RSPAMD_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "RSPAMD_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
