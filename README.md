# k8s RSpamd

Forked from https://github.com/rspamd/rspamd

Build with:

```
$ make build
```

Run with:

```
$ make run
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

|    Variable name          |    Description                 | Default   |
| :------------------------ | ------------------------------ | --------- |
|  `CONTROLLER_PASSWORD`    | Rspamd Controller Worker Pass  | `secret`  |
|  `CONTROLLER_PORT`        | Rspamd Controller Worker Port  | `11334`   |
|  `GREYLIST_SCORE`         | Rspamd Greylist Score          | `3`       |
|  `HEADER_SCORE`           | Rspamd Add-Header Score        | `4`       |
|  `NORMAL_PORT`            | Rspamd Normal Worker Port      | `11333`   |
|  `OPENPHISH_PREMIUM_FEED` | OpenPhish Premium Feed URL     | undef     |
|  `OPENPHISH`              | Enable OpenPhish community     | undef     |
|  `PROXY_PORT`             | Rspamd Proxy Worker Port       | `11332`   |
|  `REDIS_DATABASE`         | Rspamd Redis Database          | `1`       |
|  `REDIS_HOST`             | Rspamd Redis host (if no repl) | undef     |
|  `REDIS_READ_HOST`        | Rspamd Redis RO host           | undef     |
|  `REDIS_TIMEOUT`          | Rspamd Redis Timeout           | `30s`     |
|  `REDIS_WRITE_HOST`       | Rspamd Redis RW host           | undef     |
|  `REJECT_SCORE`           | Rspamd Rejection Score         | `15`      |
|  `SENTINEL_HOST`          | Rspamd Sentinel host           | undef     |
|  `SENTINEL_PORT`          | Rspamd Sentinel host           | `26379`   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point     | Description                  |
| :---------------------- | ---------------------------- |
|  `/etc/spamassassin`    | Optional SpamAssassin rules  |
|  `/var/lib/rspamd`      | Rspamd runtime directory     |
