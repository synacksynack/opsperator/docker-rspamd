FROM docker.io/debian:buster-slim

# Rspamd image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="Rspamd Image." \
      io.k8s.display-name="Rspamd" \
      io.openshift.expose-services="11332:tcp,11333:tcp,11334:tcp" \
      io.openshift.tags="antispam,rspamd" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-rspamd" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="3.3.2"

COPY config/* /

RUN set -x \
    echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init ca-certificates \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install Rspamd" \
    && apt-get -y install gnupg dirmngr curl \
    && curl -fsL http://rspamd.com/apt-stable/gpg.key | apt-key add - \
    && echo "deb http://rspamd.com/apt-stable/ buster main" \
	>/etc/apt/sources.list.d/rspamd.list \
    && apt-get update \
    && apt-get install -y rspamd \
    && echo 'type = "console";' >/etc/rspamd/override.d/logging.inc \
    && echo "# Fixing Permissions" \
    && mkdir -p /run/rspamd \
    && chown -R 1001:0 /etc/rspamd /var/lib/rspamd /run/rspamd \
    && chmod -R g=u /etc/rspamd /var/lib/rspamd /run/rspamd \
    && echo "# Cleaning Up" \
    && apt-get -y remove --purge gnupg dirmngr curl \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT [ "dumb-init", "--", "/run-rspamd.sh" ]
