#!/bin/sh

HELO=${HELO:-relay.demo.local}
FROM=${FROM:-faust64@gmail.com}
TO=${TO:-mailbox@demo.local}
MAIL_RELAY=${MAIL_RELAY:-bluemind.demo.local}
(
    sleep 2
    echo HELO $HELO
    sleep 2
    echo MAIL FROM: $FROM
    sleep 2
    echo RCPT TO: $TO
    sleep 2
    echo DATA
    sleep 2
    cat <<EOF
Subject: testing stuffnthing 
From: $FROM
To: $TO
Hey,

Hope you're not dead
stuff stuff stuff totally legit message and all

it's over 9000
.
EOF
    sleep 2
    echo quit
) | telnet $MAIL_RELAY 25

echo "=> You may now check $TO inbox"

exit 0
