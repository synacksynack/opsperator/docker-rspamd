#!/bin/sh

CONTROLLER_PASSWORD=${CONTROLLER_PASSWORD:-secret}
CONTROLLER_PORT=${CONTROLLER_PORT:-11334}
GREYLIST_SCORE=${GREYLIST_SCORE:-3}
HEADER_SCORE=${HEADER_SCORE:-4}
NORMAL_PORT=${NORMAL_PORT:-11333}
PROXY_PORT=${PROXY_PORT:-11332}
REJECT_SCORE=${REJECT_SCORE:-15}

if test "$DEBUG" = sleep; then
    sleep 3600
elif test "$DEBUG"; then
    set -x
fi
if test -z "$SENTINEL_HOST"; then
    if echo "$REDIS_READ_HOST" | grep ' ' >/dev/null; then
	REDIS_READ_HOST=$(echo "$REDIS_READ_HOST" | sed 's| |,|g')
    fi
    if test "$REDIS_HOST"; then
	REDIS_READ_HOST=$REDIS_HOST
	REDIS_WRITE_HOST=$REDIS_HOST
    elif test "$REDIS_WRITE_HOST" -a -z "$REDIS_READ_HOST"; then
	REDIS_READ_HOST=$REDIS_WRITE_HOST
    elif test "$REDIS_READ_HOST" -a -z "$REDIS_WRITE_HOST"; then
	REDIS_WRITE_HOST=$(echo "$REDIS_READ_HOST" | cut -d, -f1)
    fi
fi

echo "extended_spam_headers = true;" \
    >/etc/rspamd/override.d/milter_headers.conf

HASHED=`rspamadm pw -p "$CONTROLLER_PASSWORD"`
cat <<EOF >/etc/rspamd/override.d/worker-controller.inc
bind_socket = "*:$CONTROLLER_PORT";
enable_password = "$HASHED";
password = "$HASHED";
EOF
echo "bind_socket = \"*:$NORMAL_PORT\";" \
    >/etc/rspamd/override.d/worker-normal.inc
echo "bind_socket = \"*:$PROXY_PORT\";" \
    >/etc/rspamd/override.d/worker-proxy.inc

if test -d /etc/spamassassin; then
    if test -d /etc/spamassassin/conf.d; then
	if find /etc/spamassassin/conf.d -type -f -name '*.cf' 2>/dev/null \
		| grep '[a-z]' >/dev/null; then
	    echo "sa_main = \"/etc/spamassassin/conf.d/*\";"
	fi
    fi >/etc/rspamd/override.d/spamassassin.conf
    if test -s /etc/spamassassin/local.cf; then
	echo "sa_local = \"/etc/spamassassin/local.cf\";"
    fi >>/etc/rspamd/override.d/spamassassin.conf
fi
>/etc/rspamd/override.d/spf.conf
if test "$KUBERNETES_SERVICE_HOST"; then
    echo "disable_ipv6 = true;" >>/etc/rspamd/override.d/spf.conf
fi
if test "$OPENPHISH_PREMIUM_FEED"; then
    cat <<EOF >/etc/rspamd/override.d/phishing.conf
openphish_enabled = true;
openphish_premium = true;
openphish_map = "$OPENPHISH_PREMIUM_FEED";
phishtank_enabled = false;
EOF
elif test "$OPENPHISH"; then
    cat <<EOF >/etc/rspamd/override.d/phishing.conf
openphish_enabled = true;
openphish_premium = false;
openphish_map = "https://www.openphish.com/feed.txt";
phishtank_enabled = false;
EOF
fi
if test -s /config/spf-whitelist.conf; then
    echo "whitelist = \"/config/spf-whitelist.conf\";" \
	>>/etc/rspamd/override.d/spf.conf
fi
cat <<EOF >/etc/rspamd/override.d/classifier-bayes.conf
autolearn {
    check_balance = true;
    ham_threshold = -0.5;
    min_balance = 0.9;
    spam_threshold = 6.0;
}
EOF
cat <<EOF >/etc/rspamd/override.d/actions.conf
reject $REJECT_SCORE;
add_header = $HEADER_SCORE;
greylist = $GREYLIST_SCORE;
#subject = "***SPAM*** %s"
EOF

if test "$SENTINEL_HOST"; then
    if echo "$SENTINEL_HOST" | grep : >/dev/null; then
	if test -z "$REDIS_HOST"; then
	    REDIS_HOST=$(echo "$SENTINEL_HOST" | sed 's|:[^,]*||g')
	fi
    else
	SENTINEL_HOST=$(echo "$SENTINEL_HOST" \
			| sed -e "s|,|:${SENTNEL_PORT:-26379},|g" \
			      -e "s|\(.\)$|\1:${SENTINEL_PORT:-26379}|")
    fi
    cat <<EOF >/etc/rspamd/local.d/redis.conf
db = "${REDIS_DATABASE:-1}";
sentinels = "$SENTINEL_HOST";
sentinel_watch_time = 1min;
servers = "$REDIS_HOST";
timeout = "${REDIS_TIMEOUT:-30s}";
EOF
elif test "$REDIS_WRITE_HOST"; then
    cat <<EOF >/etc/rspamd/local.d/redis.conf
db = "${REDIS_DATABASE:-1}";
read_servers = "$REDIS_READ_HOST";
timeout = "${REDIS_TIMEOUT:-30s}";
write_servers = "$REDIS_WRITE_HOST";
EOF
fi

exec /usr/bin/rspamd --no-fork
